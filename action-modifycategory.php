<?php

session_start();

$user = $_SESSION["user"];
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

require_once "creds.php";

$newcategory =  $conn -> real_escape_string($_POST["categoryname"]);
$categoryid = $conn -> real_escape_string($_POST["categoryid"]);

echo $newcategory;
echo "<br /><br />";
echo $categoryid;

$UpdateCategory = "UPDATE Categories SET CategoryName = '$newcategory' WHERE CategoryID = '$categoryid'";

if (mysqli_query($conn, $UpdateCategory)) {
    header('location: categories.php');
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

// Close the database connection.
mysqli_close($conn);

?>