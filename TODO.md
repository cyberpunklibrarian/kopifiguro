## Euphemystique

### To Do

- [x] Add image alt tags
- [x] Rename files to prevent collisions and overwrite
- [x] Add user login functionality
- [ ] Generate an RSS feed
- [ ] Add user creation/management functionality
	- [x] Add the ability to *disable* further user creation.
- [ ] Image post editing
  - [ ] Modify titles
  - [ ] Modify poster
  - [ ] Modify category
- [x] Add an admin page to manage the site
	- [ ] Add functionality to customise the site (MOSTLY DONE)
	- [x] Ability to change site name
	- [ ] Basic theme changing
- [x] Category management
	- [x] Create category
	- [x] Modify category
	- [x] Delete category
- [x] Header and subheader customisation
- [ ] Add tag functionality
	- [ ] Load images based on tags
	- [ ] Tag management
- [x] Load images based on catagories
- [ ] Load images based on upload date
- [x] Better styling on image page
	- [x] Show who posted the image
	- [x] Show image title
	- [x] Show category
	- [x] Offer ALT/TITLE tag on hover
	- [x] Show post date
	- [ ] Show tags