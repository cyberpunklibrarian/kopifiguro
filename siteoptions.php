<?php

$GetSiteName = mysqli_query($conn, "SELECT OptionValue FROM Options WHERE OptionID = 1");

while ($row = mysqli_fetch_array($GetSiteName)) {
  $SiteName = $row['OptionValue'];
}

$GetSubName = mysqli_query($conn, "SELECT OptionValue FROM Options WHERE OptionID = 2");

while ($row = mysqli_fetch_array($GetSubName)) {
  $SubName = $row['OptionValue'];
}

$GetAllowNewUsers = mysqli_query($conn, "SELECT OptionValue FROM Options WHERE OptionID = 3");

while ($row = mysqli_fetch_array($GetAllowNewUsers)) {
  $AllowNewUsers = $row['OptionValue'];
}

$GetSiteURL = mysqli_query($conn, "SELECT OptionValue FROM Options WHERE OptionID = 4");

while ($row = mysqli_fetch_array($GetSiteURL)) {
  $SiteURL = $row['OptionValue'];
}

?>