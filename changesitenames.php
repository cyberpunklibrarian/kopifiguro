<?php

include_once 'creds.php';

session_start();

$user = $_SESSION["user"];
    
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
exit;
}

include_once "siteoptions.php";

?>
          
<!doctype html>
<html lang="en" class>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title><?php echo $SiteName; ?> - Admin</title>

    <link rel="stylesheet" href="css/styles.css" media="screen">

</head>

<body>

<ol class="site-nav">
  <li class="site-nav__item site-nav__item--homepage">
    <a href="index.php"><?php echo $SiteName; ?></a>
  </li>
</ol>

<div class="main">
  <div class="container container--wide">

      <h1>Change Site Identity</h1>

      <form action="action-changesitenames.php" method="post">

            <p>
                Site Name: <input type="text" name="sitename" id="sitename"><br><br>

                Subheading: <input type="text" name="subname" id="subname"><br><br>

                <input type="submit" value="Submit">

     </div>
     </div>
    </div>
</body>
</html>
