# Kopifiguro
## Simple Image Collection

Like many software projects, Kopifiguro is born from anger. I'm an image junky. I'll right-click and save any JPG, PNG, GIF, WebP, or whatever format you got so long as I like the image. Needless to say, I frequently hang around Tumblr because it's a huge image sharing social network. The problem with Tumblr, however, is that every company to ever own Tumblr has tried their best to destroy it. I'd like to think it's in good hands with Automattic, but only time will tell.

So I decided to try creating my own sort of image board, something simple, but easy to look at. Something that allows the user to share the images they love, on their own web server. I want Kopifiguro to be more than that, but you'll need to give me some time to work on it.

### System Requirements
Kopifiguro runs on a web server with a standard LAMP stack, but should work on a Windows server so long as you can run PHP. 

(Warning: This hasn't been tested yet.)

Basic server would have:
* A recent version of Linux
* PHP 7.4+
* Apache
* MariaDB or MySQL

### Warning
You may want to wait until this notice disappears before using Kopifiguro as a regular image sharing system.

This software is still under development and major features (user management, tags, etc) are missing. These will be added, and the database structure will change. I'll work hard to make sure updates do not break existing sites, but some planned database changes may require database resets. Keep an eye on the repo for updates, fixes, and changes.

### Credits

The underlying design of Kopifiguro is courtesy of the [Shutter](https://colorlib.com/wp/template/shutter/) theme by [Colorlib](https://colorlib.com/wp/). Originally downloaded from [ThemesLab](https://themeslab.org/). Shutter is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).
