<?php

session_start();

$user = $_SESSION["user"];
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

require_once "creds.php";

$CategoryName = $conn -> real_escape_string($_POST['category']);
$CategoryURL = strtolower($CategoryName);
$CategoryURL = str_replace(' ', '_', $CategoryURL);

$AddCategory = "INSERT INTO Categories (CategoryID, CategoryName, CategoryURL) VALUES (NULL, '$CategoryName', '$CategoryURL')";

if (mysqli_query($conn, $AddCategory)) {
    header('location: categories.php');
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

// Close the database connection.
mysqli_close($conn);

?>