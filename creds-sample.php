<?php

// Place these where you want and call them as an include in your .php files.

// Set your timezone as needed.
date_default_timezone_set('America/Chicago');

 // Credentials - edit these to fit your database.
$servername = "localhost";
$username = "database user";
$password = "database user password"; 
$database = "kopifiguroe";

// Create a database connection
$conn = new mysqli($servername, $username, $password, $database);

?>
