<?php

session_start();

$user = $_SESSION["user"];
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

require_once "creds.php";

$SiteName = $conn -> real_escape_string($_POST['sitename']);
$SubName = $conn -> real_escape_string($_POST['subname']);

?>

<!doctype html>
<html lang="en" class>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title><?php echo $SiteName; ?> - Admin</title>

    <link rel="stylesheet" href="css/styles.css" media="screen">

</head>

<body>

<ol class="site-nav">
  <li class="site-nav__item site-nav__item--homepage">
    <a href="index.php"><?php echo $SiteName; ?></a>
  </li>
</ol>

<div class="main">
  <div class="container container--wide">

<h1>Changing Site Names</h1>

<?php
$ChangeSiteName = "UPDATE Options SET OptionValue = '$SiteName' WHERE OptionID = 1";

if (mysqli_query($conn, $ChangeSiteName)) {
    echo "<p><strong>Success:</strong> Sitename updated to ".$SiteName."<br /><br />";
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

$ChangeSubName = "UPDATE Options SET OptionValue = '$SubName' WHERE OptionID = 2";

if (mysqli_query($conn, $ChangeSubName)) {
    echo "<strong>Success:</strong> Subheading updated to ".$SubName."</p>";
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

echo "<p><a href=\"admin.php\">Back to Admin</a></p>";

// Close the database connection.
mysqli_close($conn);

?>