<?php

session_start();

$user = $_SESSION["user"];
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

include_once "creds.php";
include_once "siteoptions.php";

// Set variables.
$imagetitle = $_POST["imagetitle"];
$imagecategory = $_POST["imagecategory"];
$alttag = $_POST["alttag"];

//Prepare timestamp.
$tstamp = date("YmdHis");   

// Snag the image category from the database.
$GetImageCategoryID = mysqli_query($conn,"SELECT CategoryID FROM Categories WHERE CategoryName = '$imagecategory'");
$ImageCategoryID = mysqli_fetch_row($GetImageCategoryID);

// Ensure the image category ID is correct.
// echo "Image Category ID: ".$ImageCategoryID[0];
// echo "<br /><br />";

// Prepare and upload file to /uploads.
if(isset($_FILES['image'])){
        $errors= array();
        $file_name = $_FILES['image']['name'];
        $file_size =$_FILES['image']['size'];
        $file_tmp =$_FILES['image']['tmp_name'];
        $file_type=$_FILES['image']['type'];   
        $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
        $newfile_name = $ImageCategoryID[0]."-".$tstamp.".".$file_ext;
        
        $expensions= array("jpeg","jpg","png"); 		
        if(in_array($file_ext,$expensions)=== false){
            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
        }
        if($file_size > 8000000){
        $errors[]='File size must be less than 2 MB';
        }				
        if(empty($errors)==true){
            move_uploaded_file($file_tmp,"uploads/".$newfile_name);
            //echo "Slide created:";
        }else{
            print_r($errors);
        }
    }             

// Put the information into the database.

$GetUser = mysqli_query($conn,"SELECT UserID FROM  Users WHERE User = '$user'");
$GetUserID = mysqli_fetch_array($GetUser);

//echo "User ID: ".$GetUserID[0];

$AddImage = "INSERT INTO  Images (ImageID,ImageName,ImageFile,AltTag,Category,PostedBy) VALUES (NULL, '$imagetitle', '$newfile_name', '$alttag', '$ImageCategoryID[0]', '$GetUserID[0]')";

if (mysqli_query($conn, $AddImage)) {
    echo "<p><strong>Success:</strong> Image added to database.</p>";
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

// Close the database connection.
mysqli_close($conn);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $SiteName." - ".$SubName; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/fancybox.min.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  

  <div class="site-wrap">

  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <!-- BEGIN SIDEBAR -->
  <header class="header-bar d-flex d-lg-block align-items-center" data-aos="fade-left">
    <div class="site-logo">
      <a href="index.php"><?php echo $SiteName; ?></a>
    </div>
    
    <div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

    <div class="main-menu">
      <ul class="js-clone-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Archive</a></li>
      </ul>
      <h5>Categories</h5>
      <ul class="js-clone-nav">
      <?php
          $category = mysqli_query($conn, "SELECT CategoryName FROM  Categories ORDER BY CategoryName");

            while ($row = mysqli_fetch_array($category)) {
              echo "<li><a href=\"viewcategory.php?cat=".$row['CategoryName']."\">". $row['CategoryName'] . "</a></li>";
            }
        ?>
      <h5>Site admin</h5>
      <?php

  if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == !true) { ?>
    
    <li><a href="login.php">Login</a></li></ul>
  
    <?php } else { ?>
    <li><a href="addimage.php">Add Image</a></li>
    <li><a href="admin.php">Admin</a></li>
    <li><a href="logout.php">Logout</a></li></ul>
  <?php } ?>
      
      </ul>
      <ul class="social js-clone-nav">
        <li><a href="#"><span class="icon-facebook"></span></a></li>
        <li><a href="#"><span class="icon-twitter"></span></a></li>
        <li><a href="#"><span class="icon-instagram"></span></a></li>
      </ul>
    </div>
  </header>
  
  <!-- END SIDEBAR -->
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        
        <div class="col-md-6 pt-4">
        <figure class="mb-5" data-aos="fade-up">
            <?php echo '<img src="uploads/'.$newfile_name.'" alt="'.$alttag.'" class="img-fluid">'; ?>
          </figure>
          <h2 class="text-white mb-4" data-aos="fade-up">Image Added</h2>

          <div class="row" data-aos="fade-up">
            <div class="col-md-12">

                <?php  
                // Ensure the proper data.
                echo "<p><strong>Image title:</strong> ".$imagetitle.'</p>';
                echo "<strong>Image category:</strong> ".$imagecategory.'</p>';
                echo "</p>";

                

                ?>
            </div>
          </div>
        </div>

      </div>

      <div class="row justify-content-center">
        <div class="col-md-12 text-center py-5">
          <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>.
Downloaded from <a href="https://themeslab.org/" target="_blank">Themeslab</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
        </div>
      </div>
    </div>
  </main>

</div> <!-- .site-wrap -->

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/jquery.fancybox.min.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>