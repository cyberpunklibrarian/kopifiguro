<?php

require_once "creds.php";
require_once "siteoptions.php";

if (mysqli_connect_errno($conn)) {
    echo "Database connection error:" . mysqli_connect_errno();
}

$sql = "SELECT * FROM RSS ORDER BY RSSID DESC LIMIT 30";
$query = mysqli_query($conn,$sql);

header( "Content-type: text/xml");

echo "<?xml version='1.0' encoding='UTF-8'?>
 <rss version='2.0'>
 <channel>
 <title>$SiteName</title>
 <link>$SiteURL</link>
 <description>$SubName</description>
 <language>en-us</language>";

while($row= mysqli_fetch_array($query)) {
    $rssID = $row["RSSID"];
    $ImageName = $row["ImageName"];
    $ImageFile = $row["ImageFile"];
    $AltTag = $row["AltTag"];
    $Category = $row["Category"];
    $PostedBy = $row["PostedBy"];
    $UploadDateTime = $row["UploadDateTime"];

    echo "<item>
    <imagetitle>$ImageName</imagetitle>
    <imagefile>$ImageFile</imagefile>
    <content type=\"html\">&lt;a href=&quot;$SiteURL/uploads/$ImageFile&quot;&gt; &lt;img src=&quot;$SiteURL/uploads/$ImageFile&quot;&gt;&lt;/a&gt;</content>
    <category>$Category</category>
    <posted>$PostedBy</posted>
    <uploaddate>$UploadDateTime</uploaddate>
    </item>";
}
 echo "</channel></rss>";
 ?>