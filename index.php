<?php

include_once "creds.php";

session_start();

$user = $_SESSION["user"];

include_once "siteoptions.php";

// This will be useful later...
$imgsize = array(8,4,3,6,3,6,6,4,4,4,4,8,4,4,4,6,6,3,6,3,8,4);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $SiteName." - ".$SubName; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/fancybox.min.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  

  <div class="site-wrap">

  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <!-- BEGIN SIDEBAR -->
  <header class="header-bar d-flex d-lg-block align-items-center" data-aos="fade-left">
    <div class="site-logo">
      <a href="index.php"><?php echo $SiteName; ?></a>
    </div>
    
    <div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

    <div class="main-menu">
      <ul class="js-clone-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Archive</a></li>
      </ul>
      <h5>Categories</h5>
      <ul class="js-clone-nav">
      <?php
          $category = mysqli_query($conn, "SELECT CategoryName FROM  Categories ORDER BY CategoryName");

            while ($row = mysqli_fetch_array($category)) {
              echo "<li><a href=\"viewcategory.php?cat=".$row['CategoryName']."\">". $row['CategoryName'] . "</a></li>";
            }
        ?>
      <h5>Site admin</h5>
      <?php

  if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == !true) { ?>
    
    <li><a href="login.php">Login</a></li></ul>
  
    <?php } else { ?>
    <li><a href="addimage.php">Add Image</a></li>
    <li><a href="admin.php">Admin</a></li>
    <li><a href="logout.php">Logout</a></li></ul>
  <?php } ?>
      
      </ul>
      <ul class="social js-clone-nav">
        <li><a href="#"><span class="icon-facebook"></span></a></li>
        <li><a href="#"><span class="icon-twitter"></span></a></li>
        <li><a href="#"><span class="icon-instagram"></span></a></li>
      </ul>
    </div>
  </header>
  
  <!-- END SIDEBAR -->

  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row align-items-stretch">

      <?php

      $GetImages = mysqli_query($conn,"SELECT i.ImageID, i.ImageName, i.ImageFile, i.AltTag, i.NSFW, i.UploadDateTime, c.CategoryName, u.User FROM  Images AS i JOIN  Categories AS c ON i.Category = c.CategoryID JOIN Users AS u ON i.PostedBy = u.UserID ORDER BY i.UploadDateTime DESC LIMIT 36");
      
      

      while ($row = mysqli_fetch_array($GetImages)) {
      
        echo '<div class="col-6 col-md-6 col-lg-3" data-aos="fade-up">';
          //echo '<a href="uploads/'.$row['ImageFile'].'" class="d-block photo-item">';
          if ($row['NSFW'] != 0) {
          echo '<a href="image.php?image='.$row['ImageID'].'" class="d-block photo-item" style="filter: blur(15px);">';
          } else {
          echo '<a href="image.php?image='.$row['ImageID'].'" class="d-block photo-item">';    
          }
            echo '<img src="uploads/'.$row['ImageFile'].'" alt="'.$row['AltTag'].'" class="img-fluid">';
            echo '<div class="photo-text-more">';
              echo '<div class="photo-text-more">';
              echo '<h3 class="heading">'.$row['ImageName'].'</h3>';
              echo '<span class="meta">'.$row['UploadDateTime'].' | '.$row['CategoryName'].'</span>';
            echo '</div>';
            echo '</div>';
          echo '</a>';
        echo '</div>';
        }
      ?>

      </div>

      <div class="row justify-content-center">
        <div class="col-md-12 text-center py-5">
          <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>.
Downloaded from <a href="https://themeslab.org/" target="_blank">Themeslab</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
        </div>
      </div>
    </div>
  </main>

</div> <!-- .site-wrap -->

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/jquery.fancybox.min.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>