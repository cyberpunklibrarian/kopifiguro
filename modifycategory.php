<?php

include_once 'creds.php';

session_start();

$user = $_SESSION["user"];

include_once "siteoptions.php";

$cat = htmlspecialchars($_GET["cat"]);

?>

<!doctype html>
<html lang="en" class>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title><?php echo $SiteName." - ".$SubName; ?></title>
  <link rel="stylesheet" href="css/styles.css" media="screen">
</head>

<body>

<ol class="site-nav">
  <li class="site-nav__item site-nav__item--homepage">
    <a href="index.php"><?php echo $SiteName; ?></a>
  </li>
  <li class="site-nav__item">
    &nbsp;
  </li>
  <?php

  if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == !true) { ?>
    <li class="site-nav__item">
    &nbsp;
    </li>
    <li class="site-nav__item">
      <a href="login.php">Login</a>
    </li>
    </ol>
  <?php } else { ?>
    <li class="site-nav__item">
      <a href="addimage.php">Add Image</a>
    </li>
    <li class="site-nav__item">
      <a href="admin.php">Admin</a>
    </li>
    <li class="site-nav__item">
      <a href="logout.php">Logout</a>
    </li>
    </ol>
  <?php } ?>

<div class="main">

<?php
$GetCategoryID = mysqli_query($conn,"SELECT CategoryID from Categories WHERE CategoryName = '$cat'");
$CatID = mysqli_fetch_row($GetCategoryID);
?>

  <div class="container container--wide">

      <p style="font-size:22px; font-weight:bold;">Rename <?php echo $cat; ?></p>

      <form action="action-modifycategory.php" method="post" enctype="multipart/form-data">
    <p>
        
        Category ID: <input type="hidden" name="categoryid" id="categoryid" value="<?php echo $CatID[0];?>"><?php echo $CatID[0];?><br /><br />

        Rename category to: <input type="text" name="categoryname" id="categoryname"><br /><br />    

        <input type="submit" value="Submit">
    </p>
</form>

  </div> 
</div> 

</body>
</html>