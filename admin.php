<?php

include_once 'creds.php';
include 'siteoptions.php';

session_start();

$user = $_SESSION["user"];
    
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
exit;
}

?>
          
<!doctype html>
<html lang="en" class>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title><?php echo $SiteName; ?> - Admin</title>

    <link rel="stylesheet" href="css/styles.css" media="screen">

</head>

<body>

<ol class="site-nav">
  <li class="site-nav__item site-nav__item--homepage">
    <a href="index.php"><?php echo $SiteName; ?></a>
  </li>
</ol>

<div class="main">
  <div class="container container--wide">

      <h1>Site Admin</h1>

    <div class="image-grid are-images-unloaded" data-js="image-grid">
      <div class="image-grid__col-sizer"></div>
      <div class="image-grid__gutter-sizer"></div>

        <h4>Site Identity</h4>

            <p><strong>Site Name:</strong> <?php echo $SiteName; ?><br />
            <strong>Subheading:</strong> <?php echo $SubName; ?><br />
            <strong><a href="changesitenames.php">Change site name and/or subheading</a></strong></p>

        <h4>Cateogry Management</h4>

          <p><a href="categories.php">Add, Remove and Modify Categories</a></p>

        <h4>User Management</h4>

            <?php
            if ($AllowNewUsers == 0) {
              echo "<p>New user registration: <font color=\"red\">Disabled</font> (<a href=\"action-newusers.php\">Change</a>)</p>";
            } else {
              echo "<p>New user registration: <font color=\"green\">Enabled</font> (<a href=\"action-newusers.php\">Change</a>)</p>";
            } 
            ?>
            <p>Create new user</p>
            <p>Modify user</p>
            <p>Delete user</p>

        <h4>Tag Management</h4>

            <p>Tags to appear here.</p>



        <p><a href="logout.php">Logout</a></p>     

     </div>
     </div>
    </div>
</body>
</html>
